#!/bin/bash

# Check if chromedriver exists
if [ -f "chromedrivers" ]; then
    echo "Chromedriver already in repo"
else
    # Check CPU Architecture
    echo "Checking CPU architecture and downloading chromedriver"
    if [[ $(uname -a) =~ "arm" ]]; then
        echo "Recognized ARM CPU - Downloading ARM based chromedriver"
        $(wget https://github.com/electron/electron/releases/download/v12.0.9/chromedriver-v12.0.9-linux-armv7l.zip)
        unzip chromedriver-v12.0.9-linux-armv7l.zip -d ../. && rm -rf chromedriver-v12.0.9-linux-armv7l.zip
        echo "Downloading ARM dependencies"
        apt-get install build-essential libffi-dev libssl-dev libpulse0 pulseaudio libnss3-dev -y
	apt-get install chromium-common=90.0.4430.212-1 chromium=90.0.4430.212-1 -y
    else
        echo "ARM CPU not recognized - Downloading x86_64 based chromedriver"
        $(wget https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_linux64.zip)
        unzip chromedriver_linux64.zip -d ../. && rm -rf chromedriver_linux64.zip
        apt-get install chromium-common=90.0.4430.212-1 chromium=90.0.4430.212-1 -y
    fi
fi
