#!/bin/bash

# Check CPU Architecture
echo "Checking CPU architecture and running docker container"
if [[ $(uname -a) =~ "arm" ]]; then
    echo "Recognized ARM CPU - Using ARM image"
    docker run --rm -it -v $(pwd):/usr/src/app stickyaf/docker-lv:arm-stable python3 LV.py
else
    echo "ARM CPU not recognized - Using x86_64 image"
    docker run --rm -it -v $(pwd):/usr/src/app stickyaf/docker-lv:1.1.0 python3 LV.py
fi
