'''Helper class that sends crafted messages to Slack'''

import datetime
import json
import os
import sys
import requests
import yaml

try:
    config = yaml.safe_load(open("config.yaml"))
    WEBHOOK = config['WEBHOOK']
    USER = config['USER']
except:
    raise Exception("Not able to open config.yaml")

def notify(message_text: str, no_errors: bool, ping: bool = False):
    '''Receive product info and send Slack message if webpage not found,
    item is in stock, or heartbeat when time limit is reached'''
    current_time = get_time()
    info = "\n" + str(current_time) + "\n" + message_text
    title = ("Error viewing item :cry:")
    message = ("@" + USER + info)
    if no_errors:
        title = ("Back in stock :fire:")
        message = ("@" + USER + info)
    if ping:
        title = ("Items are still out of stock :socks:")
        message = (info + '\n' + "Just letting you know I'm still working")
    slack_data = {
        "username": "NotificationBot",
        "icon_emoji": ":handbag:",
        "link_names": "1",
        "attachments": [
            {
                "color": "#9733EE",
                "fields": [
                    {
                        "title": title,
                        "value": message,
                        "short": "false",
                    }
                ]
            }
        ]
    }
    send_notification(slack_data)

def test_notification() -> int:
    '''Initial message to slack when the script starts'''
    current_time = get_time()
    title = "Initial Notification"
    message =  str(current_time) + '\n' "Letting you know the script has started"
    slack_data = {
        "username": "NotificationBot",
        "icon_emoji": ":handbag:",
        "link_names": "1",
        "attachments": [
            {
                "color": "#9733EE",
                "fields": [
                    {
                        "title": title,
                        "value": message,
                        "short": "false",
                    }
                ]
            }
        ]
    }
    return send_notification(slack_data)

def send_notification(slack_data) -> int:
    '''Reach out to slack webook and send message in slack_data'''
    print("Sending Slack Notification")
    byte_length = str(sys.getsizeof(slack_data))
    headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
    response = requests.post(WEBHOOK, data=json.dumps(slack_data), headers=headers)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.status_code

def get_time() -> str:
    '''Gets current time in YYYY-MM-DD 00:00:00 format'''
    current_time = datetime.datetime.now()
    current_time = str(current_time)[:19]
    return current_time
