'''Know when Louis Vuitton items are back in stock online'''

import datetime
import logging
import os
import random
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait

import slack_helper as slack_helper
import yaml

# Load webdriver options from config.yaml
try:
    config = yaml.safe_load(open("config.yaml"))
    HEADLESS_MODE = config['HEADLESS_MODE']
    HEARTBEAT_SECONDS = config['HEARTBEAT_SECONDS']
    DRIVER_LOCATION = config['DRIVER_LOCATION']
    URL_LIST = config['URL_LIST']
except:
    raise Exception("Not able to open config.yaml")

# Webdriver xpaths
ITEM_NAME_XPATH = '/html/body/div[1]/div/div/main/div/div[1]/div[1]/div[2]/h1'
PRICE_XPATH = '/html/body/div[1]/div/div/main/div/div[1]/div[1]/div[2]/div[3]/div[2]/div/span'
WOMENS_STOCK_XPATH = '/html/body/div[1]/div/div/main/div/div[1]/div[1]/div[2]/div[3]/div[2]/span'
MENS_STOCK_XPATH = '/html/body/div[1]/div/div/main/div/div[1]/div[1]/div[2]/div[4]/div[2]/span'

# URL Links to items on LV website
ALL_MONOGRAM = 'https://us.louisvuitton.com/eng-us/products/montsouris-pm-monogram-canvas-natural-leather-nvprod2700027v'
BLACK_MONOGRAM = 'https://us.louisvuitton.com/eng-us/products/montsouris-pm-monogram-canvas-colored-leather-nvprod2400027v'
TURTLEDOVE = 'https://us.louisvuitton.com/eng-us/products/montsouris-pm-nvprod2350004v'
BLACK_TURTLEDOVE = 'https://us.louisvuitton.com/eng-us/products/montsouris-pm-nvprod2350004v#M45205'
TINY_BACKPACK_IN_STOCK = 'https://us.louisvuitton.com/eng-us/products/tiny-backpack-monogram-empreinte-nvprod3020052v'
URL_IN_STOCK = 'https://us.louisvuitton.com/eng-us/products/lv-initiales-40mm-reversible-monogram-macassar-001129#M9821V'
URL_ERROR = 'https://us.louisvuitton.com/eng-us/products/multiple-wallet-monogram-shadow-leather-nvp'
URL_WOMEN_IN_STOCK = 'https://us.louisvuitton.com/eng-us/products/lockme-tender-lockme-nvprod2900158v#M58557'

def cop():
    '''Main function. Will call check if item if is in stock and notify the user via slack.
    If it's not in stock, will continue to check the stock and notify the user via slack heartbeat
    until time limit is reached or the item comes back in stock'''
    # Set up webdriver options
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36'
    options = Options()
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_argument('user-agent={0}'.format(user_agent)) #TODO cycle through random user_agents for less visibility
    # Chromedriver options for docker container
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    if HEADLESS_MODE:
        options.add_argument('--headless')
        options.add_argument('--window-size=1920x1080')

    # Execute driver
    serv = Service(DRIVER_LOCATION)
    driver = webdriver.Chrome(service=serv, options=options)
    wait = WebDriverWait(driver, 20)
    print("Checking if item is in stock...\n")

    # Make a list of products to visit in url_list variable
    # url_list = ALL_MONOGRAM, BLACK_MONOGRAM, TURTLEDOVE, BLACK_TURTLEDOVE
    url_count = 0
    url_size = len(URL_LIST)

    # Timestamps start time using epoch seconds
    start_time = get_epoch_seconds()
    for i in range(100 * url_size):
        # Continuously cycle through the list of products
        if url_size == url_count:
            url_count = 0
        current_url = URL_LIST[url_count]

        # Navigate to the website, 3 second sleep to load webpage
        driver.get(current_url)
        sleep(3)
        # Notify slack the script has started
        if i == 0:
            message_text = 'Script has started...'
            response = slack_helper.test_notification()
            # Functional test - will pass if able to reach out to slack
            try:
                if os.environ["TESTMODE"] == "True":
                    print("Entering Test Mode")
                    if response != 200:
                        print("Failed was not able to reach out to slack API")
                        raise Exception(response.status_code, response.text)
                    print("Success was able to reach out to slack API")
                    return True
            except:
                pass

        # Get the product name, price, and check if it's in stock
        try:
            stock_check = str(driver.find_element(By.XPATH, WOMENS_STOCK_XPATH).text)
            price_check = str(driver.find_element(By.XPATH, PRICE_XPATH).text)
            name_check =  str(driver.find_element(By.XPATH, ITEM_NAME_XPATH).text)
        except:
            # Log error and notify user via slack something wrong has happened with url
            logging.error("Not able to find xpath element")
            message_text = 'ERROR' + "\n" + current_url
            slack_helper.notify(message_text, False)
            return
        # Send slack item info
        message_text = name_check + "\n" + price_check + "\n" + stock_check + "\n" + current_url
        # Send slack chat to update user program is working but items still out of stock
        notification_time = get_epoch_seconds()
        if start_time + HEARTBEAT_SECONDS < notification_time:
            print("time to ping")   # Resets heartbeat starttime after sending slack message
            start_time = get_epoch_seconds()
            slack_helper.notify(message_text, True, True)

        # Print product information to console
        print(get_time())
        print("Item Name: " + name_check)
        print("Price: " + price_check)
        print("Stock Status: " + stock_check)
        if stock_check == 'In stock':
            # WIN
            slack_helper.notify(message_text, True)
            return
        # For the user to know how many times we visited the website in one session
        if i != 0 and (i % 5) == 0:
            print ("Queued website " + str(i) + " times")
        url_count = url_count + 1
        print('')
        sleep(random.randrange(3,7))
    driver.close()

def get_time() -> str:
    '''Gets current time in YYYY-MM-DD 00:00:00 format'''
    current_time = datetime.datetime.now()
    current_time = str(current_time)[:19]
    return current_time

def get_epoch_seconds() -> int:
    '''Gets epoch time in seconds'''
    epoch_time = datetime.datetime.now().timestamp()
    epoch_time = int(str(epoch_time)[:10])
    return epoch_time


if __name__ == "__main__":
    cop()
    