# FirstLV

## Name
FirstLV - For getting that Louis First

## Author
Tim Phimmasone @stickyaf

## Description
This program will Slack notify the user when their Louis Vuitton item is available/back in stock on the LV website. This is a Personal Project for me to develop the following skills and also attempt to get a present for my special someone.
 - Selenium
 - Python
 - Docker
 - CI/CD pipelines
   - Static Scans
   - Build
   - Test
   - Deploy (Automatically to Dockerhub)

## Installation and Usage

To run this script in a docker container, build and run the docker container. It will get all the dependencies you'll need. It also works on both x86_64 and ARM CPUs (Recommended)
 - docker build -t docker-lv .
 - docker run docker-lv python3 LV.py

To run this script on your local machine, download the dependencies, chromedriver to src/chromedriver and execute the script. If it's still not working, make sure your chromedriver is compatible with your chrome/chromium browser.
 - x86_64 (https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_linux64.zip)
 - ARM (https://github.com/electron/electron/releases/download/v12.0.9/chromedriver-v12.0.9-linux-armv7l.zip)

## Roadmap
- Webdriver class
- Slack Notification (DONE)
- Create CI pipeline (DONE)
- Create CD pipeline (DONE)
- Create Config File (DONE)
- Dockerize container (DONE)
- Works on ARM & x86_64 CPUs (DONE)
