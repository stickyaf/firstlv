FROM python:3.9.9-slim-bullseye

# Skip Rust compiler needed for Cryptography to install Selenium
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

# Set time zone
ENV TZ=America/Chicago

# Set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# Install dependencies
RUN apt-get update
RUN apt-get install wget unzip -y
COPY src/dependencies .

# Check CPU architecture and download chromedriver
RUN ./chromedriver_setup.sh

# Install pip packages
RUN pip install -r requirements.txt

# Copy python code into docker container
# COPY src/code .